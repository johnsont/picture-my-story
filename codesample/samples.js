/* Sample of code created for check pricing page
 */
 //for checking url
var param = ""; 
var checkedCache = false;

//Creating leg variables
var ARRV_DATE = new Array();
var ARRV_TIME = new Array();
//LEG_DATA stores user-input for each leg
var LEG_DATA = {};
var TOTAL_COST = new Array();
var leg = 1;
var unfinishedLeg = false;

//Function to remove a flight leg from the trip
function removeLeg() {
	if(leg > 1) {
		var prevLeg = leg - 1;
		//Remove summary for previous leg and show input, remove current leg
		$("#leg-summary-"+prevLeg).hide();
		$("#leg-summary-"+leg).remove();
		$("#hr-line-"+leg).remove();
		$("#leg"+leg).remove();
		$("#leg"+prevLeg).show();
		$(".leg-toggle-add").show();
		
		//Remove data for current leg
		delete LEG_DATA[leg];
		
		leg--;
		
		if(leg == 1){//Reset page back to default
			if(!unfinishedLeg){
				$("#running-total").html("0.00");
			}
			$("#aircraft-select").show();
			$("#trip-header").hide();
		}else{
			//Display updated total
			$("#running-total").html(TOTAL_COST[leg+1]);
		}
	}
	unfinishedLeg = false;
}
//Function to add a flight leg to the trip
function addLeg(legData) {
	//Add next leg input to page
	$.ajax({
		async: false,
		data: legData,
		type: 'GET',
		url: 'pricing/leg-add.php',
		success: function(data) {
			//callback
			$(".leg-summary:last").after(data);
		}
	});
}

$(document).ready(function() {
	
	// -------------- CHECK FOR ITN RETRIEVAL -----------------
	//Check url once onready
	if(!checkedCache){
		//variable must be a valid md5 hash
		var regex = new RegExp(/^[a-fA-F0-9]{32}$/);
		
		//getURLParameter returns url variable 'c' if it exists; function not included
		param = getURLParameter('c'); 
		if(regex.exec(param)){
			
			var c = {cache:param};
			$("#ajax-loading").show();
			
			//Send cache ID to leg-pricing.php to retrieve stored trip info
			$.getJSON('pricing/leg-pricing.php', c, function(json_data, status) {
				//Show error if detected
				if(json_data.STATUS == 'error'){
					$("#ajax-loading").hide();
					$("#legErrorMsg").html(unescape(json_data.MSG));
					$("#legErrorMsgContainer").show();
					$(".leg-toggle-add").show();
					return false;
				}
				$("#ajax-loading").hide();
				
				$("#leg-summary-"+leg).html("");
		
				var LEG_SUMMARY =  new Array();
				$("#leg-summary-"+leg).html(unescape(json_data.HTML_LEGS.leg1));
				var sn = 1;
				$.each(json_data.HTML_LEGS, function (i, html_leg_summary) {
					LEG_SUMMARY[sn] = html_leg_summary;
					sn++;
				});
				//Populate LEG_DATA
				$.each(json_data.LEGS, function (i, json_leg_data) {
					//legDept, legArrv, and legVar were all necessary to conform with the originator's page design. The departure fields have a different number ID than the arrival fields, and those ID's are used in other functions created by the originator. Some fields in the first leg do not have an ID.
					var legDept = (leg * 2) - 1;
					var legArrv = legDept + 1;
					if (leg == 1){
						legVar = "";
					}else{
						legVar = leg;
					}
					//Gathering data returned for adding the leg to the page
					LEG_DATA[leg] = {};
					LEG_DATA[leg]["L_ID1"] = json_leg_data['locationID1'];
					LEG_DATA[leg]["L_ID2"] = json_leg_data['locationID2'];
					LEG_DATA[leg]["flight_type"] = json_leg_data['itinerary_type'];
					LEG_DATA[leg]["dept_option"] = json_leg_data['outbound_sched_for'];
					LEG_DATA[leg]["dept_date"] = json_leg_data['dept_date'];
					LEG_DATA[leg]["dept_time"] = json_leg_data['dept_time'];
					LEG_DATA[leg]["retr_option"] = json_leg_data['inbound_sched_for'];
					LEG_DATA[leg]["retr_date"] = json_leg_data['retr_date'];
					LEG_DATA[leg]["retr_time"] = json_leg_data['retr_time'];
					LEG_DATA[leg]["num_passengers"] = json_leg_data['num_passengers'];
					LEG_DATA[leg]["aircraft_choice"] = json_leg_data['aircraft_choice'];
					LEG_DATA[leg]["current_leg"] = leg;
					
					//Populate fields for current leg
					if(leg == 1){
						$("#airport"+legDept).val(LEG_DATA[leg]["L_ID1"]);
					}else{
						$("#airport"+legDept).html(LEG_DATA[leg]["L_ID1"]);
					}
					
					$("#airport"+legArrv).val(LEG_DATA[leg]["L_ID2"]);
					//DEPARTURE SCHEDULE
					$("#dept-option"+legVar).val(LEG_DATA[leg]["dept_option"]);
					$("#dept-date"+legVar).val(LEG_DATA[leg]["dept_date"]);
					$("#dept-time" + legVar + " option[value='" + LEG_DATA[leg]["dept_time"] + "']").attr('selected','selected');
					//RETURN SCHEDULE
					$("#retr-option"+legVar).val(LEG_DATA[leg]["retr_option"]);
					$("#retr-date"+legVar).val(LEG_DATA[leg]["retr_date"]);
					$("#retr-time" + legVar + " option[value='" + LEG_DATA[leg]["retr-time"] + "']").attr('selected','selected');
				
					$("#num_passengers"+legVar).val();
					$("#aircraft_selected").val(LEG_DATA[leg]["aircraft_choice"]);
					
					//Hide current leg and prepare to show next leg
					$("#leg"+leg).hide();
					$("#hr-line-"+leg).show();
					$("#leg-summary-"+leg).html("");
					$("#leg-summary-"+leg).html(unescape(LEG_SUMMARY[leg]));
					$("#leg-summary-"+leg).show();
					
					//Departure airport for next leg
					var da = LEG_DATA[leg]["L_ID2"];
					//Next leg number
					var nl = leg + 1;
					var legData = {LegNum:nl, DeptApt:da};
					//Add the next leg, which will be populated until all legs are finished
					addLeg(legData);
					leg++;
				});
				//send to summary page when all legs are finished
				$('#calc-flight').trigger('click');
				
				//Update total cost
				$("#running-total").html(unescape(json_data.TOTAL_COST));
			});
		}
		//Set checkedCached when finished checking
		checkedCache = true;
	}

});
