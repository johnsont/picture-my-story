Tim Johnson's Instagram Project

Instructions:
Create an interface that receives a story from the user and then implements the Instragram API to return relevant images to the story and displays them with the story in a creative way. Please document it heavily to indicate the reason behind major coding decisions. If you elect to use a framework, please briefly explain the reason you chose the framework you did. "Familiarity" isn't an invalid answer, nor is "Curiosity.'  There is not a deadline for this though sooner is better than later.

Implementation
For the project, I decided to use the images from the Instagram API to create a background for the story. The user can hide the story and mouse over the images, reading the captions for each image. They can click on an image to follow the Instagram link. Images are also randomly replaced with unseen images. I also set up some basic CSS for a mobile version. The page is being temporarily hosted at http://hessjet.com/instagram/ if you would like to see it live.

I used HTML, CSS, PHP and Ext for the project. I decided to use PHP because I am used to programming in it. I used Ext for the first time because I remembered you talking about it and I wanted to learn how to use it. It was a great learning experience for me and I like what the framework has to offer.

Thanks,
Tim