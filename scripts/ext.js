/* Name: ext.js 
** Created by: Tim Johnson
** Purpose: 
** - Handles creation and display of the story input form
** - Collecting and submitting info to instagram-search.php
** - Displaying returned image and story data
** - Event listening for buttons
*/

Ext.BLANK_IMAGE_URL = '../images/blank.png';
Ext.require([
    'Ext.form.*'
]);
//Main image template for initial insert. Wrapped in div so that the other images do not collapse into the empty space when the images are swapped
var imageTpl = new Ext.Template('<div class="keep-position"><div id="{number}" class="image-tpl">', '<a href="{link}" class="tip has-opacity" title="{caption}" target="_blank"><img src="{thumb}"/></a>', '</div></div>');
//Template for extra images inserted with swapImages. Only includes image and link
var updateImageTpl = new Ext.Template('<a href="{link}" class="tip has-opacity" title="{caption}" target="_blank"><img src="{thumb}"/></a>');

//Set global variables:

//media will hold the images needed to display behind the story
var media;
//extra will hold the extra images returned that were not needed for displaying behind the story
var extra;
// Max # of images to display behind story
var maximum = 0;
// var to increment through extra images array while swapping images
var inc = 0;

//Task to automatically swap images after they are displayed:
//I chose to create this because I wanted to make use of the extra images found without having to display all of them at once.
//I also did not want to recycle images, so the process ends when each image has been displayed once.
var quit = false;
var swapImages = {
    run: function(){
		//Get random number within the number of images displayed. Cast toString for element usage
		var num = (Math.floor(Math.random()*maximum)).toString();
		//Only run if not called to quit and extra has another image
        if((!quit) && (extra[inc])){
			//Next image to insert
			var html = updateImageTpl.apply(extra[inc]);
			//Switch image in random image position picked
			Ext.get(num).fadeOut({
				duration: 1500,
				remove: false,
				callback: function(){
					Ext.get(num).fadeIn({duration:1500});
					Ext.get(num).dom.innerHTML = html;
					inc++;
				}
			});
        }else{
			//Stop task either if quitting or out of images
			inc = 0;
            runner.stop(swapImages);
        }
    },
    interval: 6000 //Every 6 seconds
};
var runner = new Ext.util.TaskRunner();

Ext.onReady(function(){

	var storyForm = Ext.create('Ext.form.Panel', {
        border: false,
        fieldDefaults: {
            labelWidth: 250
        },
        url: 'instagram-search.php',
        defaultType: 'textfield',
        bodyPadding: 5,
		id: 'storyForm',
		renderTo: post,

        items: [{
            name: 'title',
			maxLength: 100,
			allowBlank: false,
			validateOnBlur: false,
			emptyText: 'Enter a story title',
			height: 34,
			componentCls: 'title-input',
            anchor:'100%'
        },{
            xtype: 'textarea',
            hideLabel: true,
			allowBlank: false,
			validateOnBlur: false,
			emptyText: 'Write a short story... Then picture it!',
            name: 'story',
			height: 300,
			componentCls: 'story-textarea',
            anchor: '100%'
        }],
		buttonAlign: 'center',
        buttons: [{
            text: 'Picture My Story',
			baseCls: 'story-button',
			formBind: true,
			handler: function (btn, evt) {
				//Hide Story Form and alert div if displayed
    			Ext.get('alert').setStyle('display', 'none');
				Ext.get('post').slideOut('t',{useDisplay:true});
				Ext.get('loading').show();
				
				var data = storyForm.getForm().getValues();
				Ext.Ajax.request({
					method: 'POST',
					url: 'scripts/instagram-search.php',
					params: data,
					success: function(response, request){
						//Decode json
						var decoded = Ext.decode(response.responseText);
						
						if(decoded.error){
							//Show alert on caught error
    						Ext.get('loading').setStyle('display', 'none');
							Ext.get('post').slideIn('t',{useDisplay:true});
    						Ext.get('alert').setStyle('display', 'block');
							return false;
						}
						
						//Empty story divs
						Ext.fly('images').update("");
						Ext.fly('story-title').update("");
						Ext.fly('story-text').update("");
						
						/***********************************************************************/
						//Calculate amount of images to display based on amount of text in story:
						//I chose to do this because I wanted the display to be flexible based on how long the story is. If the story was very long, I did not want the amount of images displayed to be too small or vice-versa. This provides a rough estimate of height so that it will show just enough images to be the background of the entire story.
						
						//Total amount of images returned from Instagram API call
						var total = decoded.media.length;
						//Detect screen size to determine how many images per row to display
						if(screen.width < 800){
							//Use 3 images per row on mobile devices
							var imgPerRow = 3;
							//Rough avg px height per row for title when there are 3 images per row
							var pxPerTitleRow = 26;
							//Rough avg px height per row for story when there are 3 images per row
							var pxPerStoryRow = 38;
						}else{
							//Use 5 images per row on standard devices
							var imgPerRow = 5;
							//Rough avg px height per row for title when there are 5 images per row
							var pxPerTitleRow = 44;
							//Rough avg px height per row for story when there are 5 images per row
							var pxPerStoryRow = 90;
						}
						//Maximum of 6 rows * images per row gives limit
						var imgLimit = 6 * imgPerRow;
						//Calculate height of #image in px: 30px image container border
						var imageHeight = 30;
						//Calculate height of #story-container in px: 100x2px margin, 30x2+30px padding, 60px buttons
						var storyHeight = 350;
						
						//Add calculation for title height: each row is 31px high
						storyHeight += (Math.ceil((unescape(decoded['title']).length) / pxPerTitleRow) * 31);
						//Add calculation for story height: each row is 22px high
						storyHeight += (Math.ceil((unescape(decoded['story']).length) / pxPerStoryRow) * 22);
						
						//Number of rows to display
						var rowLimit = 0;
						//Add to row calculation until #image is large enough to hold the entire story. Limit of 30 images, or 6 rows
						while ((imageHeight < storyHeight) && (maximum < imgLimit)){
							//Each image row is 150px high
							imageHeight += 150;
							rowLimit++;
							//Max # of images to display under story: rows * # images per row 
							maximum = rowLimit * imgPerRow;
						}
						/***********************************************************************/
						
						//Put calculated number of images needed into media, then store extra for later display
						media = decoded.media.slice(0, maximum);
						extra = decoded.media.slice(maximum);
						
						//Append images to #images
						Ext.each(media, function(value) {
							imageTpl.append('images', value);
						});
						//Add formatting to the bottom
						Ext.DomHelper.append('images', {html:'<br class="clear"/>'});
						
						//Hide loading div
						Ext.get('loading').setStyle('display', 'none');
						//Open story display area
						Ext.get('story-display').show();
						//Show images
						Ext.get('images').slideIn('t',{
							useDisplay: true,
							callback: function(){
								//Set story-container over images
								Ext.get('story-container').show();
								Ext.get('story-container').setStyle('top', '-' + Ext.get('images').getHeight() + "px");
								Ext.get('story-display').setStyle('height', Ext.get('images').getHeight() + "px");
								//Add story title and text
								Ext.fly('story-title').update(unescape(decoded['title']));
								Ext.fly('story-text').update(unescape(decoded['story']));
								//Start cycle through extra images
								runner.start(swapImages);
							}
						});
					},
					failure: function(response, opts) {
						//Show alert on error
    					Ext.get('loading').setStyle('display', 'none');
						Ext.get('post').slideIn('t',{useDisplay:true});
    					Ext.get('alert').setStyle('display', 'block');
					}
				});
			}
        }]
    });
	// Display form in #post
	storyForm.show();
	
	//Hide story text so that only images are visible
	Ext.fly('story-close').on('click', function(e,t) {
    	Ext.get('bottom-button').setStyle('display', 'block');
		Ext.get('story-container').fadeOut();
    });
	
	//Bring back story text after it has been hidden
	Ext.fly('story-open').on('click', function(e,t) {
    	Ext.get('bottom-button').setStyle('display', 'none');
		Ext.get('story-container').fadeIn();
    });
	
	//Edit story by bringing back storyForm
	Ext.fly('story-edit').on('click', function(e,t) {
		Ext.get('story-display').slideOut('t', {
			useDisplay: true,
			callback: function() {
    			Ext.get('story-display').setStyle('display', 'none');
				Ext.get('post').slideIn('t',{useDisplay:true});
			}
		});
    });
});//end onReady