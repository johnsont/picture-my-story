<?php
/* Name: instagram-search.php 
** Created by: Tim Johnson (except keyword_count_sort() and extract_keywords())
** Purpose: 
** - Gets submitted story and finds keywords in the story.
** - Uses keywords as tags in Instagram API call to return relevant images.
** - Returns data in json format to be used in Ext templates.
** - To be eligible to be a keyword, a word must show up in the story more than once.
** - All words in the title are automatically eligible by being counted twice.
** - Common words specified cannot be used as keywords.
** - The amount of keywords to use for the API call is currently limited to 8 in order to reduce time.
** - Very basic error handling is included to show a simple message to the user when an error is encountered.
*/
//Get values from post
$title= stripslashes($_POST['title']);
$story = stripslashes($_POST['story']);

//Client ID for Instagram API
$client_id = "7f4d2a34e205449d8496551d8140394e";

//Uncommon words in title are automatically inluded
$keyword_search_string = $title . " " . $title . " " . $story;
//Get common words, or keywords, from story and title
$tags = extract_keywords($keyword_search_string, 3, 2, true);

//Array to store all of the images and relevent info returned
$media = array();
$i = 0;

//Basic error handling. Error message not for user display, but for debug
try {
	foreach ($tags as $tag) {
		//Send call to Instagram API to get recent images for each tag
		$contents = file_get_contents("https://api.instagram.com/v1/tags/$tag/media/recent?client_id=$client_id");
		if ($contents) {
			//Decode data and allocate array
			$json = json_decode($contents, true);
			foreach ($json["data"] as $value) {
				$media[$i]['thumb'] = $value["images"]["thumbnail"]["url"];
				$media[$i]['link'] = $value["link"];
				$media[$i]['caption'] = $value["caption"]["text"];
				$media[$i]['number'] = $i;
				$i++;
			}
		}
	}
} catch (Exception $e) {
	//If $error has any message, no images will show and the alert div will be displayed
	$error = "Error: " . $e;
}
//If no tags/images found, add to error message
if (!$media) {
	$error .= " No images found";
}
//Add data to associative array for JSON
$return_data = array(
			'title'=>$title,
			'story'=>$story,
			'media'=>$media,
			'error'=>$error
			);
//Encode for returning
$json = json_encode($return_data);
//Send back to client
echo $json;

/* These are not my functions. Copied and altered from http://snipplr.com/view/63015/ 
Used to find the most used words that are not 'common' as specified in the list.
The words returned are used as tags for the Instagram API calls. */
function keyword_count_sort($first, $sec){
	return $sec[1] - $first[1];
}
function extract_keywords($str, $minWordLen = 4, $minWordOccurrences = 2, $asArray = false, $maxWords = 8) {
    $str = str_replace(array("?","!",";","(",")",":","[","]"), " ", $str);
    $str = str_replace(array("\n","\r","  "), " ", $str);
    strtolower($str);
	$str = preg_replace('/[^\p{L}0-9 ]/', ' ', $str);
	$str = trim(preg_replace('/\s+/', ' ', $str));
	
	$words = explode(' ', $str);

	$commonWords = array('a','able','about','above','according','the','to','in','at','because','and','that','have','for','with','you','this','but','his','from', 'they','say','her','she','will','one','would','there','their','what','out','about','who','get','which','when','make','can','like','him','know','into','your','some','could','them','other','than','then','its','also','after','use','used','two','how','our','way','even','any','these','most');
	$words = array_udiff($words, $commonWords,'strcasecmp');

	$keywords = array();

	while(($c_word = array_shift($words)) !== null)
	{
		if(strlen($c_word) < $minWordLen) continue;

		$c_word = strtolower($c_word);
		if(array_key_exists($c_word, $keywords)) $keywords[$c_word][1]++;
		else $keywords[$c_word] = array($c_word, 1);
	}
	usort($keywords, 'keyword_count_sort');

	$final_keywords = array();
	foreach($keywords as $keyword_det)
	{
		if($keyword_det[1] < $minWordOccurrences) break;
		array_push($final_keywords, $keyword_det[0]);
	}
	$final_keywords = array_slice($final_keywords, 0, $maxWords);
	return $asArray ? $final_keywords : implode(', ', $final_keywords);
}
 
?>